import rpyc
import sys
from env import *

if __name__ == '__main__':
    possibleOperations = ['soma','subt', 'divi', 'mult']
    try:
        data = ' '.join(sys.argv[1:])
        operation = data.split(" ")[0]
        x = int(data.split(" ")[1])
        y = int(data.split(" ")[2])
    except :
        pass
    if not operation or operation not in possibleOperations:
        print("Uso: `python client.py <operation> <value1> <value2>")
        print("Operation: \n\tsoma - soma valor1 mais valor2 (<value1> + <value2>) \n\tsubt - subtrai valor2 de valor1 (valor1-valor2) \n\tdivi - divide valor1 por valor2 (<value1> / <value2>) \n\tmult - multiplica valor1 por valor2 (<value1> * <value2>) ")
        print("Values: \n\t<value1> é obrigatoriamente um número inteiro.\n\t<value2> é obrigatoriamente um número inteiro")
        print("Exemplos: \n\t$ python client.py soma 10 2 \n\t> 12")
        print("\n\t$ python client.py subt 10 2 \n\t> 8")
        print("\n\t$ python client.py mult 10 2 \n\t> 20")
        print("\n\t$ python client.py divi 10 2 \n\t> 5.0")
    else: 
        conn = rpyc.connect(HOST,port=PORT)
        if operation == 'soma':
            print(conn.root.soma(x,y))
        if operation == 'subt':
            print(conn.root.subt(x,y))
        if operation == 'mult':
            print(conn.root.mult(x,y))
        if operation == 'divi':
            print(conn.root.divi(x,y))
