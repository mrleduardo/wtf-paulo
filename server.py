import rpyc
from rpyc.utils.server import ThreadedServer 
from env import PORT

class Calculadora(rpyc.Service):
    def soma(self, x, y):
        print(f"{x} soma {y}")
        return x+y
    def subt(self, x, y):
        print(f"{x} subt {y}")
        return x-y
    def mult(self, x, y):
        print(f"{x} mult {y}")
        return x*y
    def divi(self, x, y):
        print(f"{x} div {y}")
        return x/y

if __name__ == "__main__":
    print("Starting server on port {}".format(PORT))
    server = ThreadedServer(Calculadora, port = PORT,  protocol_config = {"allow_public_attrs" : True})
    server.start()